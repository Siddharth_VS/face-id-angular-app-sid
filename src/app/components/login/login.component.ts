import { Component, ViewChild, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { Http } from '@angular/http';
import { UserRestService } from '../../services/user-rest.service';
import { AuthService } from '../../services/auth.service';
import { Router, NavigationExtras } from '@angular/router';
import Swal from 'sweetalert2'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  imageUrl;
  username;
  password;
  isPasswordVisible = false;
  pin;
  loginMethod: string = 'camera';

  constructor(private router: Router, private userRestService: UserRestService, private auth: AuthService, private ref: ChangeDetectorRef) { }

  ngOnInit() { }

  togglePasswordView() {
    this.isPasswordVisible = !this.isPasswordVisible;
  }

  login() {
    Swal({
      title: "Password login is disabled",
      text: 'Password login strategy has been disabled, contact administrator for details',
      type: 'info'
    })
  }


  gotoCapturePage() {
    this.router.navigate(['/capture', { source: 'login' }]);
  }
  // login method
  // login() {
  //   // set params: password or imageUrl, depending on chosen loginMethod
  //   var params: any = {
  //     username: this.username
  //   };
  //   if (this.loginMethod === 'password') {
  //     params.password = this.password;
  //   }
  //   else {
  //     params.image = this.imageUrl;
  //   }
  //   console.log('this.imageUrl', this.imageUrl)
  //   // perform http call
  //   this.userRestService.login({
  //     base64Photo: this.imageUrl.split('data:image/jpeg;base64,')[1],
  //     pin: this.pin
  //   })
  //     .subscribe(data => {
  //       // on success set token and user data
  //       var token = data.token;
  //       this.auth.setToken(token);
  //       this.auth.setUser({ username: this.username });
  //
  //       // navigate to home page
  //       this.router.navigate(['/home']);
  //     });
  // }

  // image changed handler for embedded components (image picker, camera snapshot)
  imageChanged(data) {
    this.imageUrl = data;
    this.ref.detectChanges();
  }

}
